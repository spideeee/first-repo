const mongoose = require("mongoose");

module.exports = async function transactional(models, fn){
    if (!models || models.length === 0){
        throw new Error("Do not use transactional without any model.");
    }
    if(typeof fn !== "function"){
        throw new Error("Expected function, but received " + (typeof fn));
    }
    let session = await mongoose.connection.startSession();
    session.startTransaction();
    let wrappedModels = {};
    models.forEach((model) => {
        wrappedModels[model.modelName] = wrapModel(model, session);
    });
    Symbol.asyncIterator
    return async function(...args){
        // if(fn[Symbol.toStringTag] === "AsyncFunction"){
        //     return fn(...args).then((response) => {
        //         await session.commitTransaction();
        //         session.endSession();
        //         return response;
        //     }).catch((error) => {
        //         await session.abortTransaction();
        //         throw error;
        //     });
        // }
        let response;
        try {
            if(fn[Symbol.toStringTag] === "AsyncFunction"){
                response = await fn(...args); 
            } else {
                response = fn(...args);    
            }
            await session.commitTransaction();
            session.endSession();
            return response;
        } catch (e){
            await session.abortTransaction();
            throw error;
        }
    }
}

function wrapModel(modelToWrap, session){
    function ModelWrapper(){
        throw new Error("Initialization not supported, this is a ModelWrapper for - " + modelToWrap.modelName);
    }

    Object.keys(modelToWrap).forEach((key) => {
        if(typeof modelToWrap[key] === "function"){
            ModelWrapper[key] = function(...args){
                let value = modelToWrap[key].apply(modelToWrap, ...args);
                if(typeof value.session === "function"){
                    value.session(session)
                }
                return value;
            }
        } else {
            Object.defineProperty(ModelWrapper, key, {
                get: function(){
                    return modelToWrap[key];
                },
                set: function(value){
                    modelToWrap[key] = value;
                }
            });
        }
    });

    // this function is used in place of new Model()
    ModelWrapper.new = function(...args){
        let value = new modelToWrap(...args);
        if(typeof value.$session === "function"){
            value.$session(session);
        }
        return value;
    }

    return ModelWrapper;
}