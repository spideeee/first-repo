const nodemailer = require("nodemailer");

let mailAccount = null;
async function initialiseAccount(){

    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    mailAccount = await nodemailer.createTestAccount();
}

function createMailer(){
    return async function main(to, subject, text, html){
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
          host: "smtp.ethereal.email",
          port: 587,
          secure: false, // true for 465, false for other ports
          auth: {
            user: mailAccount.user, // generated ethereal user
            pass: mailAccount.pass // generated ethereal password
          }
        });
      
        // setup email data with unicode symbols
        let mailOptions = {
          from: '"Fred Foo 👻" <foo@example.com>', // sender address
          to: to, // list of receivers
          subject: subject, // Subject line
          text: text, // plain text body
          html: html // html body
        };
      
        // send mail with defined transport object
        let info = await transporter.sendMail(mailOptions)
      
        console.log("Message sent: %s", info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    }
}

initialiseAccount();

module.exports = {
    sendMail: createMailer()
}