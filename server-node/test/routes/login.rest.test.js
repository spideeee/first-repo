//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
process.env.DB_NAME = "test";

let mongoose = require("mongoose");
let User = require('../../models/User');
let HttpStatus = require('http-status-codes');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');

chai.should();
chai.use(chaiHttp);
//Our parent block


describe('Main test suite - login.rest.js', () => {
    let registeredUser;
    before((done) => {
        registeredUser = new User({ email: "valid@email.com" });
        registeredUser.generateHash("password");
        registeredUser.save(function () {
            done();
        });
    });
    context('Test suite - POST /api/login', () => {
        let missMatchMessage = "Email or password does not match.";

        it('it should not login with out email', (done) => {
            chai.request(server)
                .post('/api/login')
                .send()
                .end((err, res) => {
                    res.should.have.status(HttpStatus.UNAUTHORIZED);
                    res.body.should.be.a('object');
                    res.body.should.have.property("message").eql(missMatchMessage);
                    done();
                });
        });

        it('it should not login with invalid email', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: "invalid-email"
                })
                .end((err, res) => {
                    res.should.have.status(HttpStatus.UNAUTHORIZED);
                    res.body.should.be.a('object');
                    res.body.should.have.property("message").eql(missMatchMessage);
                    done();
                });
        });

        it('it should not login with out a password', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: "notexist@email.com",
                })
                .end((err, res) => {
                    res.should.have.status(HttpStatus.UNAUTHORIZED);
                    res.body.should.be.a('object');
                    res.body.should.have.property("message").eql(missMatchMessage);
                    done();
                });
        });

        it('it should not login with email that does not exists', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: "notexist@email.com",
                    password: "password"
                })
                .end((err, res) => {
                    res.should.have.status(HttpStatus.UNAUTHORIZED);
                    res.body.should.be.a('object');
                    res.body.should.have.property("message").eql(missMatchMessage);
                    done();
                });
        });

        it('it should not login with incorrect password', async () => {
            try {
                let res = await chai.request(server)
                    .post('/api/login')
                    .send({
                        email: "valid@email.com",
                        password: "incorrect-password"
                    })
                res.should.have.status(HttpStatus.UNAUTHORIZED);
                res.body.should.be.a('object');
                res.body.should.have.property("message").eql(missMatchMessage);
            } catch (e) {
                console.error(e);
            }
        });

        it('it should not login if account is not activatd', async () => {
            try {
                let res = await chai.request(server)
                    .post('/api/login')
                    .send({
                        email: "valid@email.com",
                        password: "password"
                    })
                res.should.have.status(HttpStatus.UNAUTHORIZED);
                res.body.should.be.a('object');
                res.body.should.have.property("message").eql("Account not activated");
                res.body.should.have.property("notActivated").eql(true);
            } catch (e) {
                console.error(e);
            }
        });

        it('it should login with status OK', async () => {
            try {
                await registeredUser.activate();
                let res = await chai.request(server)
                    .post('/api/login')
                    .send({
                        email: "valid@email.com",
                        password: "password"
                    })
                res.should.have.status(HttpStatus.OK);
                res.body.should.be.a('object');
                res.body.should.have.property("message").eql("Logged in succesfully.");
            } catch (e) {
                console.error(e);
            }
        });
    });

    context("Test suite - GET /api/session", () => {

        it("should send unauthorized", async () => {
            let res = await chai.request(server)
                .get("/api/session");

            res.should.have.status(HttpStatus.UNAUTHORIZED);
        });

        it("should send OK", async () => {
            let agent = chai.request.agent(server);
            let res = await agent.post("/api/login")
                .send({
                    email: "valid@email.com",
                    password: "password"
                });

            res = await agent.get("/api/session");

            res.should.have.status(HttpStatus.OK);
        });
    });

    context("Test suite - POST /api/logout", () => {

        it("should send OK", async () => {
            let agent = chai.request.agent(server);
            let res = await agent.post("/api/logout")
                .send();

            res.should.have.status(HttpStatus.OK);
        });
    });

    after((done) => {
        server.close();
        User.deleteOne({
            email: "valid@email.com"
        }, function () {
            done();
        });
    });
});