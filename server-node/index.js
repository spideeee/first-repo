const app = require("./app");
const mongoose = require('mongoose');
const DB_NAME = process.env.DB_NAME || "test";

const server = app.listen(8080, (error) => {
    if(error){
        console.log(error);
    }
    console.log("Server listening on " + 8080);
});

mongoose.connection.once("open", () => {
    console.log("Mongodb connected.");
})
mongoose.connect('mongodb://localhost:27017/' + DB_NAME, {useNewUrlParser: true});

module.exports = server;