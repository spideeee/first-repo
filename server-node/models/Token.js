const mongoose = require("mongoose");
const uuidv1 = require("uuid/v1");

const MAX_AGE = 48 * 60 * 60 * 1000;

const TokenSchema = new mongoose.Schema({
    uid: {
        type: String,
        default: uuidv1
    },
    createDate: {
        type: Date,
        default: Date.now
    },
    email: {
        type: String,
        required: true
    }
});

TokenSchema.methods.isExpired = function(){
    let createTime = this.createDate.getTime();
    let currentTime = Date.now();
    if(currentTime - createTime >= MAX_AGE){
        return true;
    }
    return false;
}

const Token = mongoose.model("Token", TokenSchema);

module.exports = Token;