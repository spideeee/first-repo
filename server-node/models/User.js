const mongoose = require("mongoose");
const crypto = require("crypto");
const Constants = require("../utils/Constants");

const HASH_METHOD = "sha512";
const HASH_LENGTH = 1000;
const HASH_DIGESTION = 64;
const HASH_OUTPUT = "hex";

const UserSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String,
    salt: String,
    activated: {
        type: Boolean,
        default: false
    },
    lastLoginRequestTime: {
        type: Date,
        default: 0
    },
    loginRequestCount: {
        type: Number,
        default: 0
    },
    disabled: {
        type: Boolean,
        default: false
    },
    disableType: String,
    disabledAt: {
        type: Date,
        default: 0
    }
});

UserSchema.methods.generateHash = function(password){
    // creating a unique salt for a particular user 
    this.salt = crypto.randomBytes(16).toString(HASH_OUTPUT); 
  
    this.password = crypto.pbkdf2Sync(password, this.salt, HASH_LENGTH, HASH_DIGESTION, HASH_METHOD).toString(HASH_OUTPUT); 
}
UserSchema.methods.matchPassword = function(password){
    var hash = crypto.pbkdf2Sync(password, this.salt, HASH_LENGTH, HASH_DIGESTION, HASH_METHOD).toString(HASH_OUTPUT);

    return this.password === hash;
}
UserSchema.methods.activate = function(){
    this.activated = true;

    return this.save();
}
UserSchema.methods.updateLoginAttempt = function(){
    this.lastLoginRequestTime = Date.now();
    this.loginRequestCount += 1;
    this.save();
    return true;
}
UserSchema.methods.maxLoginRequestReached = function(){
    return this.loginRequestCount > Constants.MAX_LOGIN_REQUEST;
}
UserSchema.methods.disableTemprorily = function(){
    this.disabled = true;
    this.disabledAt = Date.now();
    this.disableType = Constants.DISABLE_TEMPORARY;

    this.save();
}
UserSchema.methods.resetLoginAttempts = function(){
    this.loginRequestCount = 0;

    this.save();
}
UserSchema.methods.removeBan = function(save){
    this.disabled = false;
    this.disabledAt = 0;
    this.disableType = "";

    save && this.save();
}

const User = mongoose.model("User", UserSchema);

module.exports = User;