const express = require('express');
const router = express.Router();
const User = require("../models/User");
const HttpStatus = require('http-status-codes');
const StringUtil = require("../utils/StringUtil");
const Constants = require("../utils/Constants");
/**
 * 
 */
router.post('/login', async function(req, res, next) {
  let email = req.body.email;
  let password = req.body.password;

  /**
   * Check if any parameter is missing
   */
  if(!email || !StringUtil.validateEmail(email) || !password){
    sendMismatchResponse(res);
    return false;
  }

  // if(req.session.isvalid){
  //   console.log("Already active session");
  // }

  try{
    let user = await User.findOne({email: email}).exec();
    /**
     * If user does not exist OR password does not match
     */
    if(!user){
      sendMismatchResponse(res);
    } else {
      /**
       * Found user, now update login attempt time
       */
      let previousLoginTime = user.lastLoginRequestTime;
      let currentTime = Date.now();
      let isLoginAttemptGapValid = (currentTime - previousLoginTime) > 1000;

      if(user.disabled == true){
        if(user.disableType = Constants.DISABLE_TEMPORARY && (currentTime - user.disabledAt) > (5 * 60 * 1000)){
          /**
           * Remove ban here but do not save,
           * allow it to go further to check the credentials 
           * and it will be saved there
           */
          user.removeBan(false);
        } else {
          res.status(HttpStatus.LOCKED).json({
            message: "Login disabled"
          });
          return false;
        }
      } else if(!isLoginAttemptGapValid && user.maxLoginRequestReached()){
        user.disableTemprorily();
        res.status(HttpStatus.LOCKED).json({
          message: "Login disabled",
          bruteForce: true
        });
        return false;
      }
      
      if(!user.matchPassword(password)){
        // this is an attempt to login with wrong password
        user.updateLoginAttempt();
        sendMismatchResponse(res);
      } else if(user.activated === false){
        res.status(HttpStatus.UNAUTHORIZED).json({
          message: "Account not activated",
          notActivated: true
        });
        // return false;
      } else {
        user.resetLoginAttempts();
        /**
         * Update session
         */
        req.session.isvalid = true;
        req.session.user = email;
        /**
         * TO DO: implement JWT
         */
        res.status(HttpStatus.OK).json({
          message: "Logged in succesfully."
        });
      }
    }
  } catch(e){
    console.log(e);
    
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      message: "Unable to login, please try again."
    });
  }
});

function sendMismatchResponse(res){
  /**
   * A random timeout between 0-1 second to allow the attacker 
   * to understand the sequence of execution for guessing 
   * which param (username/password) is wrong
   */
  setTimeout(() => {
    res.status(HttpStatus.UNAUTHORIZED).json({
      message: "Email or password does not match."
    });
  }, Math.random() * 1000);
}

router.get("/session", function(req, res){
  if(req.session.isvalid && req.session.user){
    res.status(HttpStatus.OK).json({
      message: "Valid"
    });
  } else {
    res.status(HttpStatus.UNAUTHORIZED).json({
      message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)
    });
  }
});

router.post("/logout", async function(req, res){
  req.session.isvalid = false;
  req.session.user = null;

  req.session.regenerate(function(err){
    if(err){
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        message: "Unable to log out, please try again."
      });
      return false;
    }
    res.status(HttpStatus.OK).json({
      message: "Successfully logged out."
    });
  });

});
module.exports = router;
