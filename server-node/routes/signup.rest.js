const express = require('express');
const router = express.Router();
const User = require("../models/User");
const Token = require("../models/Token");
const HttpStatus = require('http-status-codes');
const mailer = require('./../utils/mailer');
const StringUtil = require("../utils/StringUtil");

router.post('/signup', async function(req, res, next) {
  let name = req.body.name;
  let email = req.body.email;
  let password = req.body.password;
  let confirmPassword = req.body.confirmPassword;

  /**
   * Check if any parameter is missing
   */
  if(!password || !name || !email || !confirmPassword){
    res.status(HttpStatus.BAD_REQUEST).json({
      message: HttpStatus.getStatusText(HttpStatus.BAD_REQUEST)
    });
    return false;
  }

  /**
   * Check if passwords match
   */
  if(password !== confirmPassword){
    res.status(HttpStatus.NOT_ACCEPTABLE).json({
      message: "Passwords do not match."
    });
    return false;
  }

  /**
   * Validate email
   */
  if(!StringUtil.validateEmail(email)){
    res.status(HttpStatus.NOT_ACCEPTABLE).json({
      message: "Invalid email address."
    });
    return false;
  }

  /**
   * Check email existense in our DB
   */
  let userInDB = null;
  try{
    userInDB = await User.findOne({email: email}).exec();
    if(userInDB !== null && userInDB.activated === true){
      res.status(HttpStatus.FORBIDDEN).json({
        message: "Email already used"
      });
      return false;
    }
  } catch(e){
    console.log("Error finding user from db");
    console.log(e);
  }

  /**
   * All OK, now create a new user in DB
   */
  let user = null;
  try{
    if(userInDB !== null){
      user = userInDB;
    } else {
      user = new User({
        name,
        email,
      });
      user.generateHash(password);
      await user.save();
    }
    res.status(HttpStatus.OK).json({
      message: "Signup Successfull, please follow the intructions sent on " + email + " to activate your account."
    });

    // let token = new Token();
    // try{
    let token = await Token.create({
      email: email
    });
    console.log(token.uid);

    // Send email to user with the activation link
    let serverUrl = process.env.SERVER_URL;
    let activationLink = serverUrl + "accountActivation/?tokenId=" + token.uid;
    let mailContent = "We have receive a signup request from your email address. Click <a href="+activationLink+">here</a> to activate you account.";
    await mailer.sendMail(
        email,
        "Account activation.",
        mailContent,
        mailContent
    );
    
  } catch(e){
    console.log(e);

    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      message: "Unable to signup, please try again."
    });
  }
});

router.post("/activation/:tokenUid", async function(req, res){
    let tokenUid = req.params.tokenUid;

    let token = null;
    let user = null;
    try{
        token = await Token.findOne({uid: tokenUid}).exec();
        if(token === null || token.isExpired()){
            res.status(HttpStatus.BAD_REQUEST).json({
                message: "Invalid/expired URL."
            });
            return false;
        }
        user = await User.findOne({email:token.email}).exec();
        if(!user || user.activated){
            res.status(HttpStatus.BAD_REQUEST).json({
                message: "Invalid/expired URL."
            });
            return false;
        }

        await user.activate();

        res.status(HttpStatus.OK).json({
            message: "Account uccessfully activated."
        });
        token.remove();

    } catch(e){
        console.log(e);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
            message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
        });
    }
});

module.exports = router;
