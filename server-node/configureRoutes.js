module.exports = function(app){
    
    // app.use('/', require("./routes/index"));

    app.use('/api', require("./routes/login.rest"));
    app.use('/api', require("./routes/signup.rest"));
}