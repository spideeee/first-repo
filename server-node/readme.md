make sure to install and start mongodb before running nodejs server.

1. Download and install docker on your system
2. Run command 'docker pull mongo'
3. Run command 'docker run --name {some-name} -p 27017:27017 -d mongo'
4. Run command 'docker start {some-name}
5. Run command 'node index.js'