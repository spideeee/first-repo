import React from 'react';
import className from "classnames";

export default function Button({type = "primary"}){
    
}

export function SubmitButton({type = "primary", text = "Submit", ...props}){
    return (
        <input type="submit" value={text} className={className(type, "button")} {...props} />
    )
}
