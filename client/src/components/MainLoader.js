import React from "react";

function MainLoader(){
    return (
        <div className="main-loader">
            <i className="fa fa-spinner" aria-hidden="true"></i>
        </div>
    )
}

export default MainLoader;