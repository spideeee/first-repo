import React from "react";
import ConversationHeader from "../components/ConversationHeader";
import ConversationBox from "../components/ConversationBox";
import ConversationInputBox from "../components/ConversationInputBox";

class MainConversation extends React.Component{
    render(){
        return(
            <div className="main-conversation">
                <ConversationHeader />
                <ConversationBox />
                <ConversationInputBox />
            </div>
        )
    }
}

export default MainConversation;