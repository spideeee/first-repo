import React from "react";
import ConversationList from "../components/ConversationList";
import MainConversation from "../containers/MainConversation";

class Conversations extends React.Component{
    render(){

        return (
            <div className="conversations">
                <ConversationList />
                <MainConversation />
            </div>
        )
    }
}

export default Conversations;