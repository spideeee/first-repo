import React from "react";
import {Switch, withRouter, Route} from "react-router-dom";
import MainLoader from "../components/MainLoader";
import { sessionRequest } from "../utils/RESTClient";

class Authenticator extends React.Component{
    state = {
        loading: true,
        isAuthenticated: null
    }
    componentDidMount(){
        // this.
        // setTimeout(() => {
        //     this.setState({
        //         loading: false,
        //         isAuthenticated: false
        //     });
        //     // this.props.history.replace("/login");
        // }, 2000);
        this.checkSession();
    }
    async checkSession(){
        let isAuthenticated = false;
        try {
            await sessionRequest();
            isAuthenticated = true;
            this.setState({
                isAuthenticated,
                loading: false
            });
        } catch(e) {
            console.log(e);
            this.props.history.replace("/login");
        } finally {
        }
    }
    render(){
        let { loading, isAuthenticated } = this.state;

        if(loading){
            return <MainLoader />
        }

        return (
            <Switch>
                <Route path="/app" component={() => "nested home"} />
                <Route path="/app/abcd" component={() => "abcd"} />
            </Switch>
        )
    }
}


export default withRouter(Authenticator);