import React, { Component } from 'react';
import { withRouter, Route } from "react-router-dom";
import Sidebar from './components/Sidebar';
import Conversations from './containers/Conversations';
import Authenticator from './containers/Authenticator';
import Login from './views/Login';
import MainLoader from './components/MainLoader';
import { sessionRequest } from './utils/RESTClient';
// import './App.css';

class App extends Component {
  state = {
    isAuthenticated: false,
    loading: false
  }
  componentDidMount(){
    // this.checkSession();
  }
  async checkSession(){
    let isAuthenticated = false;
    try {
        await sessionRequest();
        isAuthenticated = true;
    } catch(e) {
        console.log(e);
    } finally {
      this.setState({
          isAuthenticated,
          loading: false
      }, () => {
        if(!isAuthenticated){
          this.props.history.replace("/login");
        } else if(this.props.location.pathname.startsWith("/login")){
          this.props.history.replace("/app");
        }
      });
    }
  }
  render() {
    const {loading} = this.state;
    return (
        <div className="App">
          {/* <Sidebar />
          <Route exact path="/conversations" component={Conversations} /> */}
          {/* <Authenticator /> */}
          {
            // loading
            // ?
            // <MainLoader />
            // :
            routes
          }
        </div>
    );
  }
}

export default withRouter(App);

const routes = (
  <>
    <Route path="/app" component={Authenticator} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/abcd/efgh" component={() => "404"} />
  </>
)