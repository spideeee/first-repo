import React from "react";
import className from "classnames";
import { Link, withRouter } from "react-router-dom";
import { loginRequest, signupRequest } from "../utils/RESTClient";
import {SubmitButton} from "../components/Button";

class Login extends React.Component{
    state = {
        errorMessage: null
    }
    constructor(props){
        super(props);
        this.onSignup = this.onSignup.bind(this);
        this.onSignin = this.onSignin.bind(this);
    }
    componentWillUnmount(){
        //notification.removeAll();
    }
    async onSignup(e){
        this.setState({errorMessage: null});
        e.preventDefault();
        let body = {
            name: e.target.querySelector("[name='name']").value,
            email: e.target.querySelector("[name='email']").value,
            password: e.target.querySelector("[name='password']").value,
            confirmPassword: e.target.querySelector("[name='confirmPassword']").value
        }
        try{
            let response = await signupRequest(body);
            console.log(response);
        } catch(e){
            console.dir(e);
        }
    }
    async onSignin(e){
        this.setState({errorMessage: null});
        e.preventDefault();
        let body = {
            email: e.target.querySelector("[name='email']").value,
            password: e.target.querySelector("[name='password']").value,
            remember: e.target.querySelector("[name='remember']").checked
        }
        try{
            let response = await loginRequest(body);
            console.log(response);
            this.props.history.push("/app");
        } catch(e){
            // console.log(e);
            // if(e.status == 401){
            //     notification.error({
            //         message: e.data.message,
            //         duration: 0
            //     });
            // }
            if(e.data && e.data.message){
                this.setState({errorMessage: e.data.message});
            }
        }
    }

    render(){
        let signup = this.props.location.search.indexOf("signup") !== -1;
        let {errorMessage} = this.state;
        return (
            <div className="login-wrapper">
                {/* <div className="success"></div>
                <div className="error"></div> */}
                <div className="box">
                    <div className={
                        className("login", {"active": !signup})
                    }>
                        <div>
                            <div className="create-account-button">
                                <Link to="login?signup">Create an account</Link>
                            </div>
                        </div>
                        <div>
                            <h2>Sign in</h2>
                            <form onSubmit={this.onSignin}>
                                <div className="form-group">
                                    <div className="input-box">
                                        <i className="fas fa-envelope"></i>
                                        <input type="email" placeholder="Email" name="email"/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-box">
                                        <i className="fas fa-lock"></i>
                                        <input type="password" placeholder="Password" name="password"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <input type="checkbox" name="remember" /> 
                                    <span>Remember me</span>
                                </div>

                                <div className="form-group">
                                    {/* <input type="submit" value="Sign in"/>  */}
                                    <SubmitButton text="Sign in" />
                                </div>
                                <div className="error-msg">
                                    {errorMessage || null}
                                </div>
                            </form>
                            <div className="oauth-signin">
                                Or sign in with &nbsp;<i className="fab fa-facebook"></i> &nbsp;<i className="fab fa-google-plus-square"></i> 
                            </div>
                        </div>
                    </div>
                    <div className={
                        className("signup", {"active": signup})
                    }>
                        <div>
                            <h2>Sign up</h2>
                            <form onSubmit={this.onSignup}>
                                <div className="form-group">
                                    <div className="input-box">
                                        <i className="fas fa-user"></i>
                                        <input type="text" placeholder="Name" name="name"/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-box">
                                        <i className="fas fa-envelope"></i>
                                        <input type="email" placeholder="Email" name="email"/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-box">
                                        <i className="fas fa-lock"></i>
                                        <input type="password" placeholder="Password" name="password"/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="input-box">
                                        <i className="fas fa-lock"></i>
                                        <input type="password" placeholder="Confirm password" name="confirmPassword"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <input type="checkbox" name="agreeCheck" /> 
                                    <span>I agree to all statements in term and services</span>
                                </div>

                                <div className="form-group">
                                    {/* <input type="submit" value="Sign up"/>  */}
                                    <SubmitButton text="Sign up" />
                                </div>
                            </form>
                        </div>
                        <div>
                            <div className="create-account-button">
                                <Link to="login">I am alread a member</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Login);