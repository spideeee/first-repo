import axios from "axios";
import * as urls from "../constants/Urls";

const RESTClient = axios.create({
    baseURL: urls.baseURL
});

RESTClient.interceptors.response.use((e) => {
    console.log(e);
    return e.data;
}, (e) => {
    //console.dir(e);
    throw e.response;
});

export default RESTClient;

export function loginRequest(body){
    return RESTClient.post(urls.login, body);
}

export function signupRequest(body){
    return RESTClient.post(urls.signup, body);
}

export function sessionRequest(){
    return RESTClient.get(urls.session);
}