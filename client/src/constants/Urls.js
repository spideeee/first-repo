export const baseURL = "/api";
export const login = "/login";
export const signup = "/signup";
export const session = "/session";